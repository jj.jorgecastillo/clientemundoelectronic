import React, { useState, useEffect } from 'react';
import ProductsDataService from '../../services/ProductsService';

const Products = (props) => {
  const initialProductsState = {
    id: null,
    title: '',
    description: '',
    published: false,
  };
  const [currentProducts, setCurrentProducts] = useState(initialProductsState);
  const [message, setMessage] = useState('');

  const getProducts = (id) => {
    ProductsDataService.get(id)
      .then((response) => {
        setCurrentProducts(response.data);
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    getProducts(props.match.params.id);
  }, [props.match.params.id]);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setCurrentProducts({ ...currentProducts, [name]: value });
  };

  const updatePublished = (status) => {
    var data = {
      id: currentProducts.id,
      title: currentProducts.title,
      description: currentProducts.description,
      published: status,
    };

    ProductsDataService.update(currentProducts.id, data)
      .then((response) => {
        setCurrentProducts({ ...currentProducts, published: status });
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const updateProducts = () => {
    ProductsDataService.update(currentProducts.id, currentProducts)
      .then((response) => {
        console.log(response.data);
        setMessage('The products was updated successfully!');
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const deleteProducts = () => {
    ProductsDataService.remove(currentProducts.id)
      .then((response) => {
        console.log(response.data);
        props.history.push('/tutorials');
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <div>
      {currentProducts ? (
        <div className="edit-form">
          <h4>Productos</h4>
          <form>
            <div className="form-group">
              <label htmlFor="title">Nombre</label>
              <input
                type="text"
                className="form-control"
                id="title"
                name="title"
                value={currentProducts.title}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="description">Descripción</label>
              <input
                type="text"
                className="form-control"
                id="description"
                name="description"
                value={currentProducts.description}
                onChange={handleInputChange}
              />
            </div>

            <div className="form-group">
              <label>
                <strong>Estado: </strong>
              </label>
              {currentProducts.published ? ' Publicado ' : ' Pendiente '}
            </div>
          </form>

          {currentProducts.published ? (
            <button className="badge badge-primary mr-2" onClick={() => updatePublished(false)}>
              No Publicar
            </button>
          ) : (
            <button className="badge badge-primary mr-2" onClick={() => updatePublished(true)}>
              Publicar
            </button>
          )}

          <button className="badge badge-danger mr-2" onClick={deleteProducts}>
            Eliminar
          </button>

          <button type="submit" className="badge badge-success" onClick={updateProducts}>
            Actualizar
          </button>
          <p>{message}</p>
        </div>
      ) : (
        <div>
          <br />
          <p>Producto...</p>
        </div>
      )}
    </div>
  );
};

export default Products;
