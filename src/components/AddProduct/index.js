import React, { useState } from 'react';
import TutorialDataService from '../../services/ProductsService';

const AddProduct = () => {
  const initialTutorialState = {
    id: null,
    title: '',
    description: '',
    published: false,
  };
  const [product, setProduct] = useState(initialTutorialState);
  const [submitted, setSubmitted] = useState(false);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setProduct({ ...product, [name]: value });
  };

  const saveTutorial = () => {
    var data = {
      title: product.title,
      description: product.description,
    };

    TutorialDataService.create(data)
      .then((response) => {
        setProduct({
          id: response.data.id,
          title: response.data.title,
          description: response.data.description,
          published: response.data.published,
        });
        setSubmitted(true);
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const newTutorial = () => {
    setProduct(initialTutorialState);
    setSubmitted(false);
  };

  return (
    <div className="submit-form">
      {submitted ? (
        <div>
          <h4>Guardado con éxito!</h4>
          <button className="btn btn-success" onClick={newTutorial}>
            Agregar
          </button>
        </div>
      ) : (
        <div>
          <div className="form-group">
            <label htmlFor="title">Nombre</label>
            <input
              type="text"
              className="form-control"
              id="title"
              required
              value={product.title}
              onChange={handleInputChange}
              name="title"
            />
          </div>

          <div className="form-group">
            <label htmlFor="description">Descripción</label>
            <input
              type="text"
              className="form-control"
              id="description"
              required
              value={product.description}
              onChange={handleInputChange}
              name="description"
            />
          </div>

          <button onClick={saveTutorial} className="btn btn-success">
            Guardar
          </button>
        </div>
      )}
    </div>
  );
};

export default AddProduct;
