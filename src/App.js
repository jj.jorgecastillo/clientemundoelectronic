import React from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import AddProduct from './components/AddProduct';
import Tutorial from './components/Product';
import TutorialsList from './components/ProductList';

function App() {
  return (
    <div>
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <a href="/products" className="navbar-brand">
          Mundo Electronic
        </a>
        <div className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link to={'/products'} className="nav-link">
              Productos
            </Link>
          </li>
          <li className="nav-item">
            <Link to={'/add'} className="nav-link">
              Agregar
            </Link>
          </li>
        </div>
      </nav>

      <div className="container mt-3">
        <Switch>
          <Route exact path={['/', '/products']} component={TutorialsList} />
          <Route exact path="/add" component={AddProduct} />
          <Route path="/products/:id" component={Tutorial} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
